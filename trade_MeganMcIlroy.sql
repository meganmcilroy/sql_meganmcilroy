--Q1. 
--SELECT t.size, t.price, t.buy FROM trade t
--JOIN position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
--WHERE p.trader_id = 1 and t.stock = 'MRK'

--Q2.
SELECT SUM(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
FROM trade t
  JOIN position p ON p.opening_trade_id = t.id OR p.closing_trade_id = t.id
WHERE p.trader_id = 1
  AND p.closing_trade_id is not null;

--Q3.
